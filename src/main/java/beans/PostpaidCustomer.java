/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class PostpaidCustomer {
    private String customerName, accountNumber, businessUnit, undertaking, 
             email, businessUnitId,  minimumPurchase, customerArrears, mobileno, 
            customerAddress, tariffCode, rate;

    @Override
    public String toString() {
        return "PostpaidCustomer{" + "customerName=" + customerName + ", accountNumber=" + accountNumber + ", businessUnit=" + businessUnit + ", undertaking=" + undertaking + ", email=" + email + ", businessUnitId=" + businessUnitId + ", minimumPurchase=" + minimumPurchase + ", customerArrears=" + customerArrears + ", mobileno=" + mobileno + ", customerAddress=" + customerAddress + ", tariffCode=" + tariffCode + ", rate=" + rate + '}';
    }

    public PostpaidCustomer() {
    }
    
    

    public PostpaidCustomer(String customerName, String accountNumber, String businessUnit, String undertaking, String email, String businessUnitId, String minimumPurchase, String customerArrears, String mobileno, String customerAddress, String tariffCode, String rate) {
        this.customerName = customerName;
        this.accountNumber = accountNumber;
        this.businessUnit = businessUnit;
        this.undertaking = undertaking;
        this.email = email;
        this.businessUnitId = businessUnitId;
        this.minimumPurchase = minimumPurchase;
        this.customerArrears = customerArrears;
        this.mobileno = mobileno;
        this.customerAddress = customerAddress;
        this.tariffCode = tariffCode;
        this.rate = rate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getUndertaking() {
        return undertaking;
    }

    public void setUndertaking(String undertaking) {
        this.undertaking = undertaking;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(String businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public String getCustomerArrears() {
        return customerArrears;
    }

    public void setCustomerArrears(String customerArrears) {
        this.customerArrears = customerArrears;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String tariffCode) {
        this.tariffCode = tariffCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }
    
    
    
}
