/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Transaction {
    
    private int id, type, amount, staff,  pos; 
    private String receipt_number, reference_number, mode_payment, date, remit, comment, status;

    @Override
    public String toString() {
        return "Transaction{" + "id=" + id + ", type=" + type + ", amount=" + amount + ", staff=" + staff + ", pos=" + pos + ", receipt_number=" + receipt_number + ", reference_number=" + reference_number + ", mode_payment=" + mode_payment + ", date=" + date + ", remit=" + remit + ", comment=" + comment + ", status=" + status + '}';
    }
    
    

    public Transaction( int type, int amount, int staff, int pos, String receipt_number, String reference_number, String comment, String status) {
        this.type = type;
        this.amount = amount;
        this.staff = staff;
        this.pos = pos;
        this.receipt_number = receipt_number;
        this.reference_number = reference_number;
        this.comment = comment;
        this.status = status;
    }

    public Transaction(int type, int amount, int staff, int pos, String receipt_number, String reference_number, String mode_payment, String comment, String status) {
        this.type = type;
        this.amount = amount;
        this.staff = staff;
        this.pos = pos;
        this.receipt_number = receipt_number;
        this.reference_number = reference_number;
        this.mode_payment = mode_payment;
        this.comment = comment;
        this.status = status;
    }

    public Transaction(int id, int type, int amount, int staff, int pos, String receipt_number, String reference_number, String mode_payment, String date, String remit, String comment, String status) {
        this.id = id;
        this.type = type;
        this.amount = amount;
        this.staff = staff;
        this.pos = pos;
        this.receipt_number = receipt_number;
        this.reference_number = reference_number;
        this.mode_payment = mode_payment;
        this.date = date;
        this.remit = remit;
        this.comment = comment;
        this.status = status;
    }

    public Transaction() {
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getReceipt_number() {
        return receipt_number;
    }

    public void setReceipt_number(String receipt_number) {
        this.receipt_number = receipt_number;
    }

    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public String getMode_payment() {
        return mode_payment;
    }

    public void setMode_payment(String mode_payment) {
        this.mode_payment = mode_payment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemit() {
        return remit;
    }

    public void setRemit(String remit) {
        this.remit = remit;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
