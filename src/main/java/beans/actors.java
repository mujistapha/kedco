/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class actors {
    
    private int staffID, meterNo, accountNumber, amount;

    public actors(int staffID, int meterNo, int accountNumber, int amount) {
        this.staffID = staffID;
        this.meterNo = meterNo;
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public actors(int staffID, int accountNumber) {
        this.staffID = staffID;
        this.accountNumber = accountNumber;
    }
    
    public actors(int staffID, int meterNo, int amount) {
        this.staffID = staffID;
        this.meterNo = meterNo;
        this.amount = amount;
    }
    
    public actors(int staffID) {
        this.staffID = staffID;
    }
    
    
    

    public int getStaffID() {
        return staffID;
    }

    public void setStaffID(int staffID) {
        this.staffID = staffID;
    }

    public int getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(int meterNo) {
        this.meterNo = meterNo;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
    
    
    
}
