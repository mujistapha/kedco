/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Penalties {
    private int id,  amount, staff, tamper;
    private String type, date, comment, status;

    public Penalties(int id, int amount, int staff, int tamper, String type, String date, String comment, String status) {
        this.id = id;
        this.amount = amount;
        this.staff = staff;
        this.tamper = tamper;
        this.type = type;
        this.date = date;
        this.comment = comment;
        this.status = status;
    }

    public Penalties(int amount, int tamper, String type, String comment) {
        this.amount = amount;
        this.tamper = tamper;
        this.type = type;
        this.comment = comment;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public int getTamper() {
        return tamper;
    }

    public void setTamper(int tamper) {
        this.tamper = tamper;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
}
