/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Location {
    
    private int id, region;
    private String csp, lga;

    public Location(int id, String csp, int region, String lga) {
        this.id = id;
        this.csp = csp;
        this.region = region;
        this.lga = lga;
    }

    public Location(String csp, int region, String lga) {
        this.csp = csp;
        this.region = region;
        this.lga = lga;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCsp() {
        return csp;
    }

    public void setCsp(String csp) {
        this.csp = csp;
    }

    public int getRegion() {
        return region;
    }

    public void setRegion(int region) {
        this.region = region;
    }

    public String getLga() {
        return lga;
    }

    public void setLga(String lga) {
        this.lga = lga;
    }
    
    
    
}
