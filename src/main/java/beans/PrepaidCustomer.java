/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class PrepaidCustomer {
    private String customerName, meterNumber, accountNumber, businessUnit, undertaking, 
            phoneNumber, email ,lastTransactionDate, minimumPurchase, mobileno, 
            customerAddress, tariffCode,rate;

    @Override
    public String toString() {
        return "Customer{" + "customerName=" + customerName + ", meterNumber=" + meterNumber + ", accountNumber=" + accountNumber + ", businessUnit=" + businessUnit + ", undertaking=" + undertaking + ", phoneNumber=" + phoneNumber + ", email=" + email + ", lastTransactionDate=" + lastTransactionDate + ", minimumPurchase=" + minimumPurchase + ", mobileno=" + mobileno + ", customerAddress=" + customerAddress + ", TariffCode=" + tariffCode + ", Rate=" + rate + '}';
    }

    public PrepaidCustomer() {
    }
    
    
    
    public PrepaidCustomer(String customerName, String meterNumber, String accountNumber, String businessUnit, String undertaking, String phoneNumber, String email, String lastTransactionDate, String minimumPurchase, String mobileno, String customerAddress, String TariffCode, String Rate) {
        this.customerName = customerName;
        this.meterNumber = meterNumber;
        this.accountNumber = accountNumber;
        this.businessUnit = businessUnit;
        this.undertaking = undertaking;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.lastTransactionDate = lastTransactionDate;
        this.minimumPurchase = minimumPurchase;
        this.mobileno = mobileno;
        this.customerAddress = customerAddress;
        this.tariffCode = TariffCode;
        this.rate = Rate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getMeterNumber() {
        return meterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        this.meterNumber = meterNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getUndertaking() {
        return undertaking;
    }

    public void setUndertaking(String undertaking) {
        this.undertaking = undertaking;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastTransactionDate() {
        return lastTransactionDate;
    }

    public void setLastTransactionDate(String lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public String getMinimumPurchase() {
        return minimumPurchase;
    }

    public void setMinimumPurchase(String minimumPurchase) {
        this.minimumPurchase = minimumPurchase;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(String TariffCode) {
        this.tariffCode = TariffCode;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String Rate) {
        this.rate = Rate;
    }
    
    
    
    
}
