/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class LoginLog {
    private int satffID, device;
    private String date;

    public LoginLog(int satffID, int device, String date) {
        this.satffID = satffID;
        this.device = device;
        this.date = date;
    }

    public LoginLog(int satffID, int device) {
        this.satffID = satffID;
        this.device = device;
    }
    
    

    public int getSatffID() {
        return satffID;
    }

    public void setSatffID(int satffID) {
        this.satffID = satffID;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
    
}
