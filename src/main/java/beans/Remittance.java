/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Remittance {
    
    private int id,  amount, staff, paid_amount;
    private String date, paid_date, status, transactions;

    public Remittance(int id, int amount, int staff, int paid_amount, String date, String paid_date, String status, String transactions) {
        this.id = id;
        this.amount = amount;
        this.staff = staff;
        this.paid_amount = paid_amount;
        this.date = date;
        this.paid_date = paid_date;
        this.status = status;
        this.transactions = transactions;
    }

    public Remittance(int amount, int staff, String transactions) {
        this.amount = amount;
        this.staff = staff;
        this.transactions = transactions;
    }

    public Remittance(int i, int i0, String waiting, String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStaff() {
        return staff;
    }

    public void setStaff(int staff) {
        this.staff = staff;
    }

    public int getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(int paid_amount) {
        this.paid_amount = paid_amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPaid_date() {
        return paid_date;
    }

    public void setPaid_date(String paid_date) {
        this.paid_date = paid_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactions() {
        return transactions;
    }

    public void setTransactions(String transactions) {
        this.transactions = transactions;
    }
    
    
    
}
