/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Tamper {
    
    private int id,  reported_by, verified_by, customer_type;
    private String date, meter_or_acc_no, report_type, comment, status;

    public Tamper(int id, int reported_by, int verified_by, int customer_type, String date, String meter_or_acc_no, String report_type, String comment, String status) {
        this.id = id;
        this.reported_by = reported_by;
        this.verified_by = verified_by;
        this.customer_type = customer_type;
        this.date = date;
        this.meter_or_acc_no = meter_or_acc_no;
        this.report_type = report_type;
        this.comment = comment;
        this.status = status;
    }

    public Tamper(int reported_by, int customer_type, String meter_or_acc_no, String report_type, String comment) {
        this.reported_by = reported_by;
        this.customer_type = customer_type;
        this.meter_or_acc_no = meter_or_acc_no;
        this.report_type = report_type;
        this.comment = comment;
    }
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReported_by() {
        return reported_by;
    }

    public void setReported_by(int reported_by) {
        this.reported_by = reported_by;
    }

    public int getVerified_by() {
        return verified_by;
    }

    public void setVerified_by(int verified_by) {
        this.verified_by = verified_by;
    }

    public int getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(int customer_type) {
        this.customer_type = customer_type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMeter_or_acc_no() {
        return meter_or_acc_no;
    }

    public void setMeter_or_acc_no(String meter_or_acc_no) {
        this.meter_or_acc_no = meter_or_acc_no;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
}
