/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author mujistapha
 */
public class Payment {
    private String merchantId, paidamount, recieptNumber, transactionDate, transactionReference, transactionStatus;

    public Payment(String merchantId, String paidamount, String recieptNumber, String transactionDate, String transactionReference, String transactionStatus) {
        this.merchantId = merchantId;
        this.paidamount = paidamount;
        this.recieptNumber = recieptNumber;
        this.transactionDate = transactionDate;
        this.transactionReference = transactionReference;
        this.transactionStatus = transactionStatus;
    }

    public Payment() {
    }
    
    

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getPaidamount() {
        return paidamount;
    }

    public void setPaidamount(String paidamount) {
        this.paidamount = paidamount;
    }

    public String getRecieptNumber() {
        return recieptNumber;
    }

    public void setRecieptNumber(String recieptNumber) {
        this.recieptNumber = recieptNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }
    
    
}
