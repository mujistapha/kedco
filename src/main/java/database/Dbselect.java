/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import beans.PostpaidCustomer;
import beans.PrepaidCustomer;
import beans.Staff;
import beans.Transaction;
import encryption.Hash;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mujistapha
 */
public class Dbselect {
    
    DbConnect db = new DbConnect();
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    
    public boolean userExist(Staff staff){
        boolean result = false;
        String password = null;
        
        try {
            Connection con = db.getConnection();
            String str = "select password from staff where id like " + staff.getId();
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
               password = rs.getString("password");
            }
            con.close();
            result = Hash.check(staff.getPassword(), password);
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    
    //The limit of the transaction
    public boolean withinlimit(int id){
        boolean result = false;
        try {
            Connection con = db.getConnection();
            String str = "SELECT sum(amount) as sum FROM transaction where staff = "+id+" and date like curdate()";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                int sum = rs.getInt("sum");
                if(sum<250000){
                    result = true;
                }
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public boolean noPendingRemit(Staff staff){
        boolean result = false;
        try {
            Connection con = db.getConnection();
            String str = "SELECT id FROM transaction where staff = "+staff.getId()+" and date < curdate()";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result = true;
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public ArrayList<Transaction> getTransactions(){
        ArrayList<Transaction> list = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT * FROM transaction";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                int type = rs.getInt("type"); 
                int amount = rs.getInt("amount");
                int staff = rs.getInt("staff");  
                int pos = rs.getInt("pos"); 
                String receipt_number = rs.getString("receipt_number");
                String reference_number = rs.getString("reference_number");
                String mode_payment = rs.getString("mode_payment");
                String date = rs.getString("date");
                String remit = rs.getString("remit");
                String comment = rs.getString("comment");
                String status = rs.getString("status");
                Transaction transaction = new Transaction(id, type, amount, staff, pos,
                        receipt_number, reference_number, mode_payment, date, remit, comment, status);
                list.add(transaction);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public ArrayList<Transaction> getTransactions(Staff searchStaff, String searchDate){
        String str = "";
        if(searchStaff.getId() != 0 && searchDate == null){
            str = "SELECT * FROM transaction where staff like " + searchStaff.getId();
        } else if (searchStaff.getId() == 0 && searchDate != null){
            str = "SELECT * FROM transaction where date like " + searchDate;
        } else {
            str = "SELECT * FROM transaction where staff like " + searchStaff.getId() + " and date like " + searchDate ;
        }
        ArrayList<Transaction> list = null;
        try {
            Connection con = db.getConnection();
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                int type = rs.getInt("type"); 
                int amount = rs.getInt("amount");
                int staff = rs.getInt("staff");  
                int pos = rs.getInt("pos"); 
                String receipt_number = rs.getString("receipt_number");
                String reference_number = rs.getString("reference_number");
                String mode_payment = rs.getString("mode_payment");
                String date = rs.getString("date");
                String remit = rs.getString("remit");
                String comment = rs.getString("comment");
                String status = rs.getString("status");
                Transaction transaction = new Transaction(id, type, amount, staff, pos,
                        receipt_number, reference_number, mode_payment, date, remit, comment, status);
                list.add(transaction);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
  
    public Transaction getTransaction(){
        Transaction transaction = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT * FROM transaction";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                int type = rs.getInt("type"); 
                int amount = rs.getInt("amount");
                int staff = rs.getInt("staff");  
                int pos = rs.getInt("pos"); 
                String receipt_number = rs.getString("receipt_number");
                String reference_number = rs.getString("reference_number");
                String mode_payment = rs.getString("mode_payment");
                String date = rs.getString("date");
                String remit = rs.getString("remit");
                String comment = rs.getString("comment");
                String status = rs.getString("status");
                transaction = new Transaction(id, type, amount, staff, pos,
                        receipt_number, reference_number, mode_payment, date, remit, comment, status);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return transaction;
    }
    
    public String getUnremittedTransactions(int id){
        String result = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT id FROM transaction where staff like "+id + " and remitted like 'no' ";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result += rs.getInt("id");
                result += ",";
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public int getUnremittedTransactionsAmount(int id){
        int result = 0;
        try {
            Connection con = db.getConnection();
            String str = "SELECT sum(amount) as amount FROM transaction where staff like "+id + " and remitted like 'no' or remitted like 'waiting' ";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getInt("amount");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public String getremittedTransactionsAmount(int id){
        String result = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT transactions FROM remittance where id like "+id;
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getString("transactions");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public int getRemitAmount(int id){
        int result = 0;
        try {
            Connection con = db.getConnection();
            String str = "SELECT amount FROM transaction where id like "+id + " ";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getInt("amount");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public PostpaidCustomer viewPostpaidCustomer(){
        PostpaidCustomer customer = null;
        return customer;
    }
    
    public PrepaidCustomer viewPrepaidCustomer(){
        PrepaidCustomer customer = null;
        return customer;
    }
    
    public ArrayList<String> getPosUsage(){
        ArrayList<String> list = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT pos, count(id) as number, DATE_FORMAT(date, \"%b %Y\") as date FROM transaction group by pos and DATE_FORMAT(date, \"%b %Y\")";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                list.add(rs.getInt("pos") + ":" + rs.getInt("count") +":"+ rs.getDate("date") );
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    
    public int getPos(String number){
        int result = 0;
        try {
            Connection con = db.getConnection();
            String str = "SELECT id FROM pos where number like '"+number+"' ";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                result = rs.getInt("id");
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    
    public ArrayList<String> numberOfTransactions(){
        ArrayList<String> list = null;
        try {
            Connection con = db.getConnection();
            String str = "SELECT type, count(id) as number, sum(amount) as amount, DATE_FORMAT(date, \"%b %Y\") "
                    + "as date FROM transaction group by type and DATE_FORMAT(date, \"%b %Y\")";
            pstmt = con.prepareStatement(str);
            rs = pstmt.executeQuery();
            while(rs.next()){
                list.add(rs.getInt("type") + ":" + rs.getInt("number") + ":" + rs.getInt("amount") + ":" + rs.getDate("date"));
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Dbselect.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
//    public ArrayList<String> getPenStats(){
//        Connection con = db.getConnection();
//        String str = "SELECT sum(amount) as sum FROM transaction where staff = "+staff.getId()+" and date like curdate()";
//        pstmt = con.prepareStatement(str);
//        rs = pstmt.executeQuery();
//        while(rs.next()){
//            int sum = rs.getInt("sum");
//            if(sum<250000){
//                result = true;
//            }
//        }
//        con.close();
//        return new ArrayList<>();
//    } 
//    
//    public ArrayList<Transaction> remitTransactions(){
//        Connection con = db.getConnection();
//        String str = "SELECT sum(amount) as sum FROM transaction where staff = "+staff.getId()+" and date like curdate()";
//        pstmt = con.prepareStatement(str);
//        rs = pstmt.executeQuery();
//        while(rs.next()){
//            int sum = rs.getInt("sum");
//            if(sum<250000){
//                result = true;
//            }
//        }
//        con.close();
//        return new ArrayList<>();
//    }
}
