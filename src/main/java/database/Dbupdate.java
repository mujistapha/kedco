/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mujistapha
 */
public class Dbupdate {
    
    DbConnect db = new DbConnect();
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    public void processRemit(String id) throws SQLException{
        Connection con = db.getConnection();
        String str =  "update transaction set remitted = 'generated' where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        pstmt.executeUpdate();
        con.close();
    }
    
    public void remit(String id) throws SQLException{
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Connection con = db.getConnection();
        String str =  "update transaction set remitted = 'generated "+date+"' where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        pstmt.executeUpdate();
        con.close();
    }
    
    public void comfirmRemit(int id, int amount) throws SQLException{
        Connection con = db.getConnection();
        String str =  "update remittance set status = 'comfirmed' paid_date = curdate() where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        pstmt.executeUpdate();
        con.close();
    }
    
    public String changePassword(String password, int id) throws SQLException{
        String result = null;
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Connection con = db.getConnection();
        String str =  "update staff set password = '"+password+"' where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        if(pstmt.executeUpdate()> 0){
            result = "Successful";
        } else {
            result = "Unsuccessful";
        }
        con.close();
        return result;
    }
    
    public String verifyTamper(int tamperID, int staffId) throws SQLException{
        String result = null;
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Connection con = db.getConnection();
        String str =  "update tamper set verified_by = "+staffId+" where id like "+tamperID+" "; 
        pstmt = con.prepareStatement(str);    
        if(pstmt.executeUpdate()> 0){
            result = "Successful";
        } else {
            result = "Unsuccessful";
        }
        con.close();
        return result;
    }
    
    public String newPosStatus(int id, String newStatus) throws SQLException{
        String result = null;
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Connection con = db.getConnection();
        String str =  "update pos set status = "+newStatus+" where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        if(pstmt.executeUpdate()> 0){
            result = "Successful";
        } else {
            result = "Unsuccessful";
        }
        con.close();
        return result;
    }
    
    public String newStaffStatus(int id, String newStatus) throws SQLException{
        String result = null;
        String date = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        Connection con = db.getConnection();
        String str =  "update staff set status = "+newStatus+" where id like "+id+" "; 
        pstmt = con.prepareStatement(str);    
        if(pstmt.executeUpdate()> 0){
            result = "Successful";
        } else {
            result = "Unsuccessful";
        }
        con.close();
        return result;
    }
    
    
    
    
}
