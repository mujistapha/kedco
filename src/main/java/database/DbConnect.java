/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mujistapha
 */
public class DbConnect {
    
    public Connection getConnection() throws SQLException{
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:Mysql://localhost:3306/kedco", "root", "");
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
        }
        return conn;
    }
    
}
