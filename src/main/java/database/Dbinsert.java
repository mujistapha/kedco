/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import beans.*;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author mujistapha
 */
public class Dbinsert {
    
    DbConnect db = new DbConnect();
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    public void newPos(Pos pos){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
            java.util.Date langDate = sdf.parse(pos.getActive_date());
            Date sqlDate = new Date(langDate.getTime());
            Connection con = db.getConnection();
            String str =  "insert into pos values(id, " + pos.getNumber() + ", '" + pos.getLocation() + "', "
                    + "' "+ sqlDate +"', '" + pos.getComment()+ "', 'inactive' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new pos.");
        } catch (ParseException ex) {
            System.out.println("Error: " + ex.getMessage() + ", trying to convert string to date. When adding new pos.");
        }
        
    }
    
    public void newStaff(Staff staff){
        try {
            Connection con = db.getConnection();
            String str =  "insert into staff values(id, '" + staff.getFirst_name() + "', '" + staff.getLast_name() + "', "
                    + " "+ staff.getPhone_number() +", '" + staff.getEmail() + "', '"+ staff.getType()+"', '"
                    + staff.getAddress()+ "', '"+ staff.getLocation()+"', '" + staff.getPassword() + "', 'inactive'  )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new staff.");
        }
    }
    
    public void newtransaction(Transaction transaction){
        try {
            Connection con = db.getConnection();
            String str =  "insert into transaction values(id, '" + transaction.getReceipt_number() + "', '" + transaction.getReference_number() + "', "
                    + " "+ transaction.getType() +", " + transaction.getAmount() + ", 'cash', curdate() , "
                    + ""+ transaction.getStaff()+", 'no', '"+ transaction.getComment() +"' , " 
                    + transaction.getPos() + ", '"+ transaction.getPos() +"' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new transaction.");
        }
    }
    
    public void newloginLog(LoginLog log){
        try {
            Connection con = db.getConnection();
            String str =  "insert into login_log values(" + log.getSatffID() + ", curdate() , '"+ log.getDevice() +"' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new login log.");
        }
        
    }
    
    public void newRegion(Region region){
        try {
            Connection con = db.getConnection();
            String str =  "insert into region values(id, '" + region.getName() + "', '"+ region.getState() +"' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new region.");
        }
    }
    
    public void newLocation(Location location){
        try {
            Connection con = db.getConnection();
            String str =  "insert into location values(id, '" + location.getCsp() + "', " + location.getRegion() + " , '"+ location.getLga() +"' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new location.");
        }
    }
    
    public int newRemittance(Remittance remittance){
        int id = 0;
        try {
            Connection con = db.getConnection();
            String str =  "insert into remittance values(id, curdate(), " + remittance.getAmount() + ", "
                    + " "+ remittance.getStaff() +", 0, null, '"
                    + remittance.getTransactions() + "', 'waiting' )";
            stmt = con.createStatement();
            int generatedId = stmt.executeUpdate(str, Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            while(rs.next()){
                id = rs.getInt(1);
            }
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new remittance.");
        }
        return id;
    }
    
    public void newTamper(Tamper tamper){
        try {
            Connection con = db.getConnection();
            String str =  "insert into tamper values(id, curdate(), '" + tamper.getMeter_or_acc_no() + "', "
                    + " "+ tamper.getReported_by() +", 0, "+ tamper.getCustomer_type() +", '"
                    + tamper.getReport_type() + "', '"+ tamper.getComment() +"', 'unverified')";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new tamper.");
        }
    }
    
    public void newPenalty(Penalties penalties){
        try {
            Connection con = db.getConnection();
            String str =  "insert into penalties values(id, '" + penalties.getType() + "', " + penalties.getAmount() + ", "
                    + " curdate(), '" + penalties.getComment() + "', "+ penalties.getTamper() +", 'waiting' )";
            pstmt = con.prepareStatement(str);
            pstmt.executeUpdate();
            con.close();
        } catch (SQLException ex) {
            System.out.println("Error: " + ex.getMessage() + ". the sql: " + ex.getSQLState() + ". When adding new penalties.");
        }
    }
    
}
