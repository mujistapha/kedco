package com.mastech.kedcoAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KedcoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KedcoApiApplication.class, args);
	}
}
