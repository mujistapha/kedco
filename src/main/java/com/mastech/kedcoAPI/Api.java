/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mastech.kedcoAPI;

import beans.*;
import database.Dbinsert;
import database.Dbselect;
import database.Dbupdate;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mujistapha
 */
public class Api {
    Dbselect dbselect = new Dbselect();
    Dbinsert dbinsert = new Dbinsert();
    Dbupdate dbupdate = new Dbupdate();
    
    /**
     * 
     * @param username
     * @param password
     * @return 0 if user do not exist, 1 if user has non-remitted transactions 
     */
    public int login(int username, String password){
        Staff staff = new Staff(username, password);
        int result = 0;
        if(dbselect.userExist(staff)){
            result++;
            if(!dbselect.noPendingRemit(staff)){
                result++;
            }   
        }return result;
    }
    
    public boolean limit(int id){
        return dbselect.withinlimit(id);
    }
    
    public PostpaidCustomer findPostpaidCustomer(int number){
        PostpaidCustomer customer = new PostpaidCustomer();
        return customer;
    }
    
    public PrepaidCustomer findPrepaidCustomer(int number){
        PrepaidCustomer customer = new PrepaidCustomer();
        return customer;
    }
    
    public void makeTransaction(int staff, int amount, int customerID, int customerType, String posID, String comment){
        int pos = dbselect.getPos(posID);
        if(customerType == 1){
            //http request
            Payment payment = new Payment();
            Transaction transaction = new Transaction(1, amount, staff, pos ,
                    payment.getRecieptNumber(), payment.getTransactionReference(), comment, payment.getTransactionStatus());
        } else {
            
        }
    }
    
    public ArrayList<Transaction>  viewTransactions(int staffID, String searchDate){
        Staff staff = new Staff(staffID);
        return dbselect.getTransactions(staff, searchDate);
    }
    
    public ArrayList<Transaction>  viewTransactions(){
        return dbselect.getTransactions();
    }
    
    public String generateRemit(int staffID) throws SQLException{
        Remittance remittance = new Remittance(dbselect.getUnremittedTransactionsAmount(staffID), staffID, dbselect.getUnremittedTransactions(staffID));
        int id = dbinsert.newRemittance(remittance);
        String[] transactions = dbselect.getUnremittedTransactions(staffID).split(",");
        for(int i = 0; i < transactions.length; i++){
            dbupdate.processRemit(transactions[i]);
        }
        return id + "," + dbselect.getUnremittedTransactionsAmount(staffID);
    }
    
    public void comfirmRemit(int id, int amount) throws SQLException{
        String trans = dbselect.getremittedTransactionsAmount(id);
        String[] transactions = trans.split(",");
        String result = "";
        if(dbselect.getRemitAmount(id) == amount){
            for(int i = 0; i < transactions.length; i++){
            dbupdate.remit(transactions[i]);
            dbupdate.comfirmRemit(id, amount);
            result = "seccussful";
            } 
        }else if (dbselect.getRemitAmount(id) < amount){
            dbupdate.comfirmRemit(id, amount);
            result = "incomplete-amount";
        } else {
            result = "unseccussful";   
        }
        
    }
    
    public int changePassword(int id, String oldPassword, String newPassword) throws SQLException{
        Staff staff = new Staff(id, oldPassword);
        int result = 0;
        if(dbselect.userExist(staff)){
            if(dbupdate.changePassword(newPassword, id).equals("Successful")){
                result++;
            }
        }return result;
    }
    
    public void reportTamper(int reported_by, int customer_type, String meter_or_acc_no, String report_type, String comment){
        Tamper tamper = new Tamper(reported_by, customer_type, meter_or_acc_no, report_type, comment);
        dbinsert.newTamper(tamper);
    }
    
    public String verifyTamper(int tamperID, int staffId) throws SQLException{
        return dbupdate.verifyTamper(tamperID, staffId);
    }
    
    public String newStaff(int phone_number, String first_name, String last_name, String email, String type, String address, 
            String location, String password) throws SQLException{
        Staff staff = new Staff(phone_number, first_name, last_name, email, type, address, location, password);
        dbinsert.newStaff(staff);
        return "Successful";
    }
    
    public String newPos(int number, String location, String active_date, String comment) throws SQLException{
        Pos pos = new Pos(number, location, active_date, comment);
        dbinsert.newPos(pos);
        return "Successful";
    }
    
    public String newRegion(String name, String state) throws SQLException{
        Region region = new Region(name, state);
        dbinsert.newRegion(region);
        return "Successful";
    }
    
    public String newLocation(String csp, int region, String lga) throws SQLException{
        Location location = new Location(csp, region, lga);
        dbinsert.newLocation(location);
        return "Successful";
    }
    
    public String changePosStatus(int id, String newStatus) throws SQLException{
        return dbupdate.newPosStatus(id, newStatus);
    }
    
    public String changeStaffStatus(int id, String newStatus) throws SQLException{
        return dbupdate.newStaffStatus(id, newStatus);
    }
    
    public void posStats(){
        
    }
    
    public void TransactionStats(){
        
    }
    
    
}
